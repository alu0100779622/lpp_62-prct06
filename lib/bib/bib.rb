module Bib

  	class Bib

		attr_accessor :autores, :titulo, :serie, :editorial, :numEdicion, :fecha, :numISBN


		def initialize(a,b,c,d,e,f,g)
			@autores = a
			@titulo = b
			@serie = c 
			@editorial = d
			@numEdicion = e
			@fecha = f
			@numISBN = g
		end

		def to_s()
			"#{autores.join(", ")}.\n#{titulo}\n(#{serie})\n#{editorial}; #{numEdicion} edicion #{fecha}\n#{numISBN.join("\n")}"

		end
		
	end

end

@L = Bib::Bib.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"],
			"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide",
			"The Facets of Ruby",
			"Pragmatic Bookshelf",4,"(July 7,2013)",
			["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])

 puts @L.to_s()
