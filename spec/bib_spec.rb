require 'spec_helper'
require 'bib'

describe Bib do
  before :each do 
		@Bib = Bib::Bib.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"],
			"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide",
			"The Facets of Ruby",
			"Pragmatic Bookshelf",4,"(July 7,2013)",
			["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
	end

	describe "# Expectativas de los atributos" do 
	  
		it "Hay uno o mas autores" do
			expect(@Bib.autores).not_to be_empty
		end
		it "Debe existir un titulo" do
			expect(@Bib.titulo.length).not_to be 0
		end
		it "Debe existir o no una serie" do
			expect(@Bib.serie.length).to be >=0
		end
		it "Debe existir una editorial" do
			expect(@Bib.editorial.length).not_to be 0
		end
		it "Debe existir un numero de edicion" do
			expect(@Bib.numEdicion.to_s.length).not_to be 0
		end
		it "Debe existir una fecha de publicacion" do
			expect(@Bib.fecha.length).not_to be 0
		end
		it "Debe existir uno o mas numeros ISBN." do
			expect(@Bib.numISBN).not_to be_empty
		end
	end
	
	describe "# Expectativas de los metodos" do 

		it "Existe un metodo para obtener el listado de autores." do
			@Bib.autores == ["Dave Thomas", "Andy Hunt", "Chad Fowler"]
		end
		it "Existe un metodo para obtener el titulo." do
			@Bib.titulo =="Programming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide"
		end
		it "Existe un metodo para obtener la serie" do
			@Bib.serie =="The Facets of Ruby"
		end
		it "Existe un metodo para obtener la editorial." do
			@Bib.editorial =="Pragmatic Bookshelf"
		end
		it "Existe un metodo para obtener el numero de edicion" do
			@Bib.numEdicion == 4
		end
		it "Existe un metodo para obtener la fecha de publicacion." do
			@Bib.fecha =="(July 7,2013)"
		end
		it "Existe un metodo para obtener el listado de ISBN" do
			@Bib.numISBN == ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"]
		end
		it "Existe un metodo para obtener la referencia formateada" do
		  expect(@Bib.to_s).to eq("Dave Thomas, Andy Hunt, Chad Fowler.\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide\n(The Facets of Ruby)\nPragmatic Bookshelf; 4 edicion (July 7,2013)\nISBN-13: 978-1937785499\nISBN-10: 1937785491")
	    end
	end
end
